## Description
Usually I put task description here

## Requirements
* Python 3.7
* HTTPie

## Usage

### Start the service
```
bash ./start.sh
```

or

```
python ./main.py
```

### Get a user info
```
http get http://127.0.0.1:5000/users/1
```

### Check login
```
http --json post http://127.0.0.1:5000/login email='user1@example.com' password="user1-password"
```

### List endpoints
```
http --json get http://127.0.0.1:5000/endpoints
```

### Create an endpoint
```
http --json post http://127.0.0.1:5000/endpoints user_id=1 uri='//endpoint2'
```

### Update an endpoint
```
http --json patch http://127.0.0.1:5000/endpoints/1 uri='//endpoint2'
```

### Delete an endpoint
```
http --json delete http://127.0.0.1:5000/endpoints/1
```
