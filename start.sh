#!/usr/bin/env bash
# Create venv
[[ ! -d venv ]] && {
    python -m venv venv
}

source venv/bin/activate
pip install -r requirements-dev.txt

python ./main.py
