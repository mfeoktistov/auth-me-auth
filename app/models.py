from sqlalchemy import TIMESTAMP, Column, ForeignKey, Integer, Text
from sqlalchemy.orm import relationship
from sqlalchemy.sql.functions import func

from .app import database


# User object should contain a name, # email address, password
# and the date of their last login.
class User(database.Model):
    id = Column(Integer, primary_key=True)

    email = Column(Text, nullable=False, unique=True)
    name = Column(Text, nullable=False)
    password = Column(Text, nullable=False)

    last_login_dt = Column(TIMESTAMP, nullable=True)

    created_at = Column(TIMESTAMP, nullable=False)
    updated_at = Column(TIMESTAMP, nullable=False, server_default=func.now())

    endpoints = relationship("Endpoint")


class Endpoint(database.Model):
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey(User.id), nullable=False)
    uri = Column(Text, unique=True)

    created_at = Column(TIMESTAMP, nullable=False)
    updated_at = Column(TIMESTAMP, nullable=False, server_default=func.now())
