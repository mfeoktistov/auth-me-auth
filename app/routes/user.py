from http import HTTPStatus

from flask import g
from flask_expects_json import expects_json
from werkzeug.security import check_password_hash

from app import app, models, schemas

# TODO: Implement CRUD for User objects


@app.route("/users/<int:user_id>", methods=["GET"])
def get_user(user_id):
    """
    Find and return a user by their id.
    :param user_id: Requested user identifier.
    :return:        JSON object of the requested user.
    """
    user = models.User.query.get_or_404(user_id)
    return dict(
        data=dict(
            id=user.id,
            name=user.name,
            email=user.email,
            endpoints=[e.uri for e in user.endpoints],
        )
    )


@app.route("/login", methods=["POST"])
@expects_json(schemas.login_schema)
def login():
    user: models.User = models.User.query.filter_by(email=g.data["email"]).one()
    is_valid = False
    status = HTTPStatus.NOT_FOUND
    last_login = None

    if user:
        # We MUST use check_password_hash to prevent time based brute force attacks
        is_valid = check_password_hash(user.password, g.data["password"])

    # Use 404 for invalid password
    if is_valid:
        status = HTTPStatus.OK
        last_login = user.last_login_dt and user.last_login_dt.isoformat()

    return dict(data=dict(is_valid=is_valid, last_login=last_login)), status


__all__ = ["login", "get_user"]
