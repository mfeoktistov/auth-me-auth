from datetime import datetime
from http import HTTPStatus

from flask import g
from flask_expects_json import expects_json

from app import app, database, models, schemas


@app.route(
    "/endpoints/<int:endpoint_id>",
    methods=["GET"],
    endpoint="get_endpoint",
)
def get_endpoint(endpoint_id: int):
    """
    Find and return a endpoint by id.
    :param endpoint_id: Requested endpoint identifier.
    :return:        JSON object of the requested user.
    """
    endpoint: models.Endpoint = models.Endpoint.query.get_or_404(endpoint_id)
    return dict(
        data=dict(
            id=endpoint.id,
            uri=endpoint.uri,
            created_at=endpoint.created_at,
            updated_at=endpoint.updated_at,
        )
    )


@app.route("/endpoints", methods=["GET"])
def list_endpoints():
    # TODO: Implement pagination
    data = [dict(id=e.id, uri=e.uri) for e in models.Endpoint.query.all()]
    return dict(data=data)


@app.route(
    "/endpoints",
    methods=["POST"],
    endpoint="create_endpoint",
)
@expects_json(schemas.create_enpoint_schema)
def create_endpoint():
    user_id = g.data["user_id"]
    uri = g.data["uri"]

    user = models.User.query.get_or_404(user_id)
    endpoint = models.Endpoint(user_id=user.id, uri=uri, created_at=datetime.utcnow())

    database.session.add(endpoint)
    database.session.commit()

    return dict(data=dict(id=endpoint.id, uri=endpoint.uri))


@app.route(
    "/endpoints/<int:endpoint_id>",
    methods=["PATCH"],
    endpoint="update_endpoint",
)
@expects_json(schemas.update_enpoint_schema)
def update_endpoint(endpoint_id: int):
    uri = g.data["uri"]
    endpoint = models.Endpoint.query.get_or_404(endpoint_id)
    endpoint.uri = uri

    database.session.add(endpoint)
    database.session.commit()

    return dict(
        data=dict(
            id=endpoint.id,
            uri=endpoint.uri,
            created_at=endpoint.created_at,
            updated_at=endpoint.updated_at,
        )
    )


@app.route(
    "/endpoints/<int:endpoint_id>",
    methods=["DELETE"],
    endpoint="delete_endpoint",
)
def delete_endpoint(endpoint_id: int):
    endpoint = models.Endpoint.query.get_or_404(endpoint_id)
    models.Endpoint.query.filter_by(id=endpoint_id).delete()
    database.session.commit()
    return dict(data=dict(success=True, id=endpoint.id))


__all__ = [
    "create_endpoint",
    "update_endpoint",
    "get_endpoint",
    "delete_endpoint",
    "list_endpoints",
]
