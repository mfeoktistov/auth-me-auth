from .app import app, database

from app.routes.user import get_user
from app.routes.endpoints import *
