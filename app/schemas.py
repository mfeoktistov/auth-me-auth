login_schema = {
    "type": "object",
    "properties": {
        "email": {"type": "string"},
        "password": {"type": "string"},
    },
    "required": ["email", "password"],
}


create_enpoint_schema = {
    "type": "object",
    "properties": {
        "user_id": {"type": "number"},
        "uri": {"type": "string"},
    },
    "required": ["uri", "user_id"],
}


update_enpoint_schema = {
    "type": "object",
    "properties": {
        "uri": {"type": "string"},
    },
    "required": ["uri"],
}
