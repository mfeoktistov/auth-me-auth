from distutils.util import strtobool
from os import environ

from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)

# For simplicity I will use in memory sqlite db
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = strtobool(
    environ.get("SQLALCHEMY_TRACK_MODIFICATIONS", "False")
)

app.config["SQLALCHEMY_DATABASE_URI"] = (
    environ.get("SQLALCHEMY_DATABASE_URI") or "sqlite:///:memory:"
)

database = SQLAlchemy(app)
