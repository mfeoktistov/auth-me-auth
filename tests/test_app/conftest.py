import pytest
from sqlalchemy.sql.functions import func
from werkzeug.security import generate_password_hash

from flask_sqlalchemy import SQLAlchemy

from app.models import Endpoint, User


@pytest.fixture(scope="function")
def user(test_database: SQLAlchemy):
    # Insert a test user.
    user = User(
        name="Test User",
        email="user@test.org",
        password=generate_password_hash("password1"),
        created_at=func.now(),
    )
    test_database.session.add(user)
    test_database.session.commit()
    return user


@pytest.fixture(scope="function")
def user_w_endpoint(test_database: SQLAlchemy, user):

    endpoint = Endpoint(
        uri="//myendpoint",
        user_id=user.id,
        created_at=func.now(),
    )

    test_database.session.add(endpoint)
    test_database.session.commit()

    return user
