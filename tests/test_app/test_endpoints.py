from app.models import Endpoint
from http import HTTPStatus

from flask_sqlalchemy import SQLAlchemy


def test_get_endpoint(client, user_w_endpoint):
    """
    Test GET endpoint route.
    :param client: Flask test client.
    """

    # Request the endpoint
    endpoint = user_w_endpoint.endpoints[0]
    response = client.get(f"/endpoints/{endpoint.id}")
    assert response.status_code == HTTPStatus.OK

    data = response.json["data"]

    assert data["id"] == endpoint.id
    assert data["uri"] == endpoint.uri


def test_get_endpoint_not_found(client):
    """
    Test GET endpoint that doesn't exist
    :param client: Flask test client.
    """

    # Request the endpoint
    response = client.get("/endpoints/-1")
    assert response.status_code == HTTPStatus.NOT_FOUND


def test_list_endpoints(client, test_database: SQLAlchemy, user_w_endpoint):
    """
    Test list endpoints route
    """
    response = client.get(f"/endpoints")
    assert response.status_code == HTTPStatus.OK

    data = response.json["data"]
    assert len(data), "Empty response"


def test_list_endpoints_empty(client):
    """
    Test list endpoints route with empty response
    """
    response = client.get(f"/endpoints")
    assert response.status_code == HTTPStatus.OK

    assert not response.json["data"]


def test_update_endpoint(client, test_database, user_w_endpoint):
    """
    Update endpoint
    """
    (endpoint_id,) = test_database.session.query(Endpoint.id).first()
    response = client.patch(f"/endpoints/{endpoint_id}", json=dict(uri="foo"))

    assert response.status_code == HTTPStatus.OK

    endpoint = test_database.session.query(Endpoint).filter_by(id=endpoint_id).one()
    assert endpoint.uri == "foo"


def test_create_endpoint(client, test_database, user):
    """
    Create endpoint
    """
    response = client.post(f"/endpoints", json=dict(user_id=user.id, uri="foo"))

    assert response.status_code == HTTPStatus.OK

    data = response.json["data"]
    endpoint_id = data["id"]

    endpoint = test_database.session.query(Endpoint).filter_by(id=endpoint_id).one()
    assert endpoint.uri == "foo"


def test_delete_endpoint(client, test_database, user_w_endpoint):
    """
    Delete endpoint
    """
    (endpoint_id,) = test_database.session.query(Endpoint.id).first()
    response = client.delete(f"/endpoints/{endpoint_id}")

    assert response.status_code == HTTPStatus.OK

    endpoint = test_database.session.query(Endpoint).filter_by(id=endpoint_id).first()
    assert endpoint is None
