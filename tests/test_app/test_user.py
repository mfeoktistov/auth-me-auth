from http import HTTPStatus


def test_get_user(client, user_w_endpoint):
    """
    Test GET user route.
    :param client: Flask test client.
    """

    # Request the user
    response = client.get(f"/users/{user_w_endpoint.id}")
    assert response.status_code == HTTPStatus.OK
    data = response.json["data"]

    assert data["id"] == user_w_endpoint.id
    assert data["name"] == user_w_endpoint.name

    endpoint = user_w_endpoint.endpoints[0]

    assert endpoint.uri in data["endpoints"]


def test_get_user_404(client):
    """
    Test GET user route for a non-existent user ID.
    :param client: Flask test client
    """
    response = client.get("/users/-1")
    assert response.status_code == HTTPStatus.NOT_FOUND
