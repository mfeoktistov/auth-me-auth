from datetime import datetime

from werkzeug.security import generate_password_hash

from app import database
from app.models import Endpoint, User


def insert_data():
    """
    Insert sample data into the database.
    """

    now = datetime.utcnow()

    # Insert users
    database.session.bulk_save_objects(
        [
            User(
                name="Test User 1",
                email="user1@example.com",
                password=generate_password_hash("user1-password"),
                last_login_dt=now,
                created_at=now,
            ),
            User(
                name="Test User 2",
                email="user2@example.com",
                password=generate_password_hash("user2-password"),
                created_at=now,
            ),
            User(
                name="Test User 3",
                email="user3@example.com",
                password=generate_password_hash("user3-password"),
                created_at=now,
            ),
        ]
    )

    database.session.bulk_save_objects(
        [
            Endpoint(
                user_id=1,
                uri="//my-service/enpoint-1",
                created_at=now,
            )
        ]
    )

    database.session.commit()
