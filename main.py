from app import app, database
from fixtures import insert_data

# Create a new database and put in some data.
# TODO: Implement deployment process
database.create_all()
insert_data()


if __name__ == "__main__":
    app.run(debug=True)
